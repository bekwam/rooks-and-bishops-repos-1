/*
 * Copyright 2016 Bekwam, Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bekwam.rnb.client;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * @author carl
 *
 */
public class RooksAndBishopsApp extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {

		FXMLLoader fxmlLoader = new FXMLLoader(RooksAndBishopsApp.class.getResource("/rnb-fxml/RooksAndBishops.fxml"));
		
		Parent p = fxmlLoader.load();
		
		RooksAndBishopsController c = fxmlLoader.getController();
		
		Scene scene = new Scene(p);
		scene.getStylesheets().add("/rnb-css/rnb.css");
		scene.setOnMouseMoved((evt) -> c.mouseMoved(evt));
		scene.setOnMouseDragged((evt)->c.mouseMoved(evt));
/*		scene.setOnMousePressed((evt) -> c.mousePressed(evt));
		scene.setOnMouseReleased((evt) -> c.mouseReleased(evt));
*/		
		primaryStage.setScene( scene );
		primaryStage.show();
	}

	public static void main(String[] args) { launch(args); }
}
